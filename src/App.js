import React, {Component} from 'react';
import './App.css';
import meatImage from './components/assets/Burger.jpeg';
import friesImage from './components/assets/fries.jpeg';
import pizzaImage from './components/assets/pizza.jpeg';
import donerImage from './components/assets/doner.jpeg';
import colaImage from './components/assets/cola.jpeg';
import juiceImage from './components/assets/juice.jpeg';
import cocktailImage from './components/assets/cocktail.jpeg';
import Menu from './components/Menu/Menu';
import Order from './components/Menu/Order'


class App extends Component {
    state = {
        items: [
            {name: 'Menu', count: 0, price: 120,image: meatImage},
            {name: 'French Fries', count: 0, price: 40,image: friesImage},
            {name: 'Doner', count: 0, price: 130,image: donerImage},
            {name: 'Pizza', count: 0, price: 400,image: pizzaImage},
            {name: 'Cola', count: 0, price: 55,image: colaImage},
            {name: 'Juice', count: 0, price: 65,image: juiceImage},
            {name: 'Cocktail', count: 0, price: 100,image: cocktailImage},
        ],
        total: 0,
    };


    addItems = (name) => {

        const items = [...this.state.items];

        let copyTotal = this.state.total;

        for(let i = 0; i < items.length; i++) {
            if(items[i].name === name) {
                items[i].count++;

                copyTotal += items[i].price;

                items[i].show = true

            }
        }

        this.setState({total: copyTotal});

    };
    removeComponent = (name) => {
        const copyForRemove = [...this.state.items];
        let copyTotal = this.state.total;

        for(let i = 0; i < copyForRemove.length; i++) {
            if(copyForRemove[i].name === name) {
                if (copyForRemove[i].count !== 0) {
                    copyForRemove[i].count--;
                    copyTotal -= copyForRemove[i].price;
                 if (copyForRemove[i].count < 1){
                        copyForRemove[i].show = false
                    }
                }
            }
        }
        this.setState({total: copyTotal})
    };


    render() {

        return (
        <div className="App container">
            <div>
            <h1>M<br/>e<br/>n<br/>u</h1>
            </div>
        <div className="AddItem">
                    <h2>Add items:</h2>
                    <Menu
                        onClickAdd={(name) => this.addItems(name)}
                        items={this.state.items}
                        remove={this.removeComponent}
                    />
                </div>
                <div className="order">
                    <h2>Order Details:</h2>
                    <Order total={this.state.total}/>
                </div>
            </div>
        );
    }
}

export default App;
