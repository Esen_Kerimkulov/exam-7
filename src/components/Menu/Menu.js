import React from 'react';
import "./Style.css"
const Menu = (props) => {

    return (
        props.items.map((fill, key) => {

            return (
                <div className="add-item" key={key}>
                    <img src={fill.image} alt="#" className="img" onClick={() => props.onClickAdd(fill.name)}/>
                    <p className="menu-name" onClick={() => props.onClickAdd(fill.name)}>{fill.name}</p>
                    <span className="count">x{fill.count}</span>

                    <span className="price">Price: {fill.price} som</span>

                    {fill.show ? <button className="delete-btn" onClick={() => props.remove(fill.name)} style={{display: 'inline-block'}}>{'\u2718'}

                    </button> : null}

                </div>
            )

        })

    )
};

export default Menu;